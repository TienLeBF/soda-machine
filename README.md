# soda-machine
# 1. deploy soda-machine on dev or prod branch
## 1.1. requirement:
    Java 8, Maven 
## 1.2. run shell
    cd soda-machine
    mvn spring-boot:run
## 1.3 use cart and payment
    send curl in file ./soda-machine.http
