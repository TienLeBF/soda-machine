package com.le.sm.model;

import com.le.sm.entity.Product;

public class ProductSum {
    private int num;
    private Product product;

    public ProductSum(int num, Product product) {
        this.num = num;
        this.product = product;
    }

    public ProductSum() {
        super();
    }

    public int getNum() {
        return this.num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
