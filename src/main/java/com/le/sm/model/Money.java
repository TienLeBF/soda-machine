package com.le.sm.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Money {
    private Money() {
    }

    public static final Integer TEN_THOUSAND_VND = 10000;
    public static final Integer TWENTY_THOUSAND_VND = 20000;
    public static final Integer FIFTY_THOUSAND_VND = 50000;
    public static final Integer ONE_HUNDRED_THOUSAND_VND = 100000;
    public static final Integer TWO_HUNDRED_THOUSAND_VND = 200000;

    public static final Integer[] LIST_MONEY = new Integer[] { Money.TWO_HUNDRED_THOUSAND_VND,
            Money.ONE_HUNDRED_THOUSAND_VND,
            Money.FIFTY_THOUSAND_VND, Money.TWENTY_THOUSAND_VND, Money.TEN_THOUSAND_VND };
    // public static final Set<Integer> ACCEPT_NOTES = Set.of(LIST_MONEY);
    public static final Set<Integer> ACCEPT_NOTES;
    public static final Map<Integer, Integer> MONEY_IN_MACHINE;
//    = Map.of(TEN_THOUSAND_VND, 10, TWENTY_THOUSAND_VND, 10,
//            FIFTY_THOUSAND_VND, 10, ONE_HUNDRED_THOUSAND_VND, 10, TWO_HUNDRED_THOUSAND_VND, 10);
    static {
        MONEY_IN_MACHINE = new HashMap<Integer, Integer>();
        MONEY_IN_MACHINE.put(TEN_THOUSAND_VND, 10);
        MONEY_IN_MACHINE.put(TWENTY_THOUSAND_VND, 10);
        MONEY_IN_MACHINE.put(FIFTY_THOUSAND_VND, 10);
        MONEY_IN_MACHINE.put(ONE_HUNDRED_THOUSAND_VND, 10);
        MONEY_IN_MACHINE.put(TWO_HUNDRED_THOUSAND_VND, 10);
        ACCEPT_NOTES = new HashSet<Integer>();
        ACCEPT_NOTES.addAll(Arrays.asList(LIST_MONEY));
    }
}
