package com.le.sm.configuration.database;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Configuration;

import com.le.sm.entity.Product;
import com.le.sm.model.Money;

@Configuration
public class ProductMagament {
    // products same as table product in DB
    public static Map<Integer, Product> products;
    public static Map<Integer, Integer> stocks;

    static {
        products = new HashMap<Integer, Product>();
        Product cake = new Product("Cake", Money.TEN_THOUSAND_VND);
        Product pepsi = new Product("Pepsi", Money.TEN_THOUSAND_VND);
        Product soda = new Product("Soda", Money.TEN_THOUSAND_VND);
        products.put(cake.getId(), cake);
        products.put(pepsi.getId(), pepsi);
        products.put(soda.getId(), soda);

        stocks = new HashMap<Integer, Integer>();
        stocks.put(cake.getId(), 10);
        stocks.put(pepsi.getId(), 10);
        stocks.put(soda.getId(), 10);
        System.out.println("stocKs: \n" + stocks);
    }
}
