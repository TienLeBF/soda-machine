package com.le.sm.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.le.sm.model.Money;

@RestController
@RequestMapping("/admin")
public class AdminController {

    /**
     * update product with id
     *
     * @param price
     */
    @PostMapping(value = "/product/update/{id}")
    public void updatePrice(@ModelAttribute Integer price, @PathVariable Integer id) {
        // Check product on database ->
        // B1. Exists ->
        // IF price not null && price > 0 -> update -> update DB -> response updated
        // ELSE -> response price invalid
        // Not exists -> response not found product ID

    }

    @GetMapping(value = "/money/cashDrawer")
    public ResponseEntity<Map<String, Object>> cashDrawer() {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            data.put("message", "successful");
            data.put("cashDrawer", Money.MONEY_IN_MACHINE);
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("message", "Something error happern");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }
}
