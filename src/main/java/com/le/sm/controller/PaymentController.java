package com.le.sm.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.le.sm.logic.PaymentLogic;
import com.le.sm.model.Money;
import com.le.sm.service.CartService;
import com.le.sm.service.PaymentService;
import com.le.sm.utils.Constants;

@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

    @Autowired
    private PaymentService service;

    @Autowired
    private CartService cartService;

    private static final AtomicInteger moneySum = new AtomicInteger(0);

    @GetMapping(value = "/add/{money}")
    public ResponseEntity<Map<String, Object>> add(@PathVariable Integer money) {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            data.put("message", "add " + money);
            boolean isAccept = PaymentLogic.acceptNote(money);
            if (isAccept) {
                Money.MONEY_IN_MACHINE.put(money, Money.MONEY_IN_MACHINE.get(money) + 1);
                PaymentController.moneySum.addAndGet(money);
                data.put("moneySum", moneySum);
                return ResponseEntity.status(HttpStatus.OK).body(data);
            }
            throw new Exception();
        } catch (Exception e) {
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

    @PostMapping(value = "/pay")
    public ResponseEntity<Map<String, Object>> pay() {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            Map<Integer, Integer> refund = this.service.pay(PaymentController.moneySum.get());
            data.put("message", "successful");
            int money = PaymentController.moneySum.get();
            data.put("moneySum", money);
            data.put("refund", refund);
            PaymentController.moneySum.set(0);
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            if (e.getMessage().equals(Constants.NOT_ENOUGH_MONEY)) {
                data.put("message", Constants.NOT_ENOUGH_MONEY);
            } else {
                data.put("message", "Something error happen");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

    @PostMapping(value = "/cancel")
    public ResponseEntity<Map<String, Object>> clear() {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            this.cartService.clear();
            Map<Integer, Integer> refund = this.service.pay(PaymentController.moneySum.get());
            data.put("message", "canceled");
            data.put("moneySum", moneySum);
            data.put("refund", refund);
            PaymentController.moneySum.set(0);
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }
}
