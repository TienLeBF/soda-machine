package com.le.sm.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.le.sm.configuration.database.ProductMagament;
import com.le.sm.entity.Product;
import com.le.sm.model.ProductSum;

@RestController
@RequestMapping("/product")
public class ProductController {
    private static final Logger LOG = Logger.getLogger(ProductController.class.getSimpleName());
    @GetMapping("/findAll")
    public ResponseEntity<Map<String, Object>> findAll() {
        LOG.info("cart findAll id is call");
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            data.put("data", "successful");
            final Map<Integer, ProductSum> products = new HashMap<Integer, ProductSum>();
            ProductSum ps = null;
            for (Entry<Integer, Product> item : ProductMagament.products.entrySet()) {
                ps = new ProductSum();
                ps.setNum(ProductMagament.stocks.get(item.getKey()));
                ps.setProduct(item.getValue());
                products.put(item.getKey(), ps);
            }
            data.put("products", products);
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }
}
