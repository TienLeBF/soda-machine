package com.le.sm.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.le.sm.configuration.database.ProductMagament;
import com.le.sm.entity.Product;
import com.le.sm.logic.CartLogic;
import com.le.sm.model.ProductSum;
import com.le.sm.service.CartService;

@RestController
@RequestMapping(value = "/cart")
public class CartController {
    private static final Logger LOG = Logger.getLogger(CartController.class.getSimpleName());

    @Autowired
    private CartService service;

    @GetMapping(value = "/findAll")
    public ResponseEntity<Map<String, Object>> findAll() {
        LOG.info("cart findAll is call");
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            final Map<Integer, ProductSum> products = new HashMap<Integer, ProductSum>();
            ProductSum ps = null;
            for (Entry<Product, Integer> item : CartLogic.products.entrySet()) {
                ps = new ProductSum();
                ps.setNum(item.getValue());
                ps.setProduct(item.getKey());
                products.put(item.getKey().getId(), ps);
            }
            data.put("products", products);
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

    @PostMapping(value = "/add/{id}")
    public ResponseEntity<Map<String, Object>> addProduct(@PathVariable Integer id) {
        LOG.info("cart add id is call");
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            Product product = ProductMagament.products.get(id);
            boolean isSuccess = this.service.addProduct(product);
            if (isSuccess) {
                data.put("message", "Added a Product " + product.getName() + "  to Cart.");
                return ResponseEntity.status(HttpStatus.OK).body(data);
            }
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

    @PostMapping(value = "/decre/{id}")
    public ResponseEntity<Map<String, Object>> decreProduct(@PathVariable Integer id) {
        LOG.info("cart decre id is call");
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            Product product = ProductMagament.products.get(id);
            boolean isSuccess = this.service.decreProduct(product);
            if (isSuccess) {
                data.put("message", "Decreased a Product " + product.getName() + "  from Cart.");
                return ResponseEntity.status(HttpStatus.OK).body(data);
            }
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        } catch (Exception e) {
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

    @PostMapping(value = "/remove/{id}")
    public ResponseEntity<Map<String, Object>> removeProduct(@PathVariable Integer id) {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            Product product = ProductMagament.products.get(id);
            boolean isSuccess = this.service.removeProduct(product);
            if (isSuccess) {
                data.put("message", "Remove Product " + product.getName() + "  from Cart.");
                return ResponseEntity.status(HttpStatus.OK).body(data);
            }
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        } catch (Exception e) {
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }

//    @PostMapping(value = "/cancel")
    public ResponseEntity<Map<String, Object>> clear() {
        Map<String, Object> data = new HashMap<String, Object>();
        try {
            this.service.clear();
            data.put("message", "cancel Cart.");
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            data.put("message", "Something error happen");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data);
        }
    }
}
