package com.le.sm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Product implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = -4469147699647680094L;
    private static AtomicInteger autoId = new AtomicInteger(0);
    private int id;
    private String name;
    private int price;
    private Date updatedAt;
    private Date createdAt;

    public Product() {
        this.id = Product.autoId.getAndIncrement();
    }

    public Product(String name, int price) {
        this.id = Product.autoId.getAndIncrement();
        System.out.println("name = " + name + "id = " + this.id);
        this.name = name;
        this.price = price;
        this.createdAt = new Date();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
