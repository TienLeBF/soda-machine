package com.le.sm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.le.sm.entity.Product;
import com.le.sm.logic.CartLogic;
import com.le.sm.service.CartService;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartLogic logic;

    @Override
    public boolean addProduct(Product product) {
        return this.logic.addProduct(product);
    }

    @Override
    public boolean increProduct(Product product) {
        return this.logic.increProduct(product);
    }

    @Override
    public boolean decreProduct(Product product) {
        return this.logic.decreProduct(product);
    }

    @Override
    public boolean removeProduct(Product product) {
        return this.logic.removeProduct(product);
    }

    @Override
    public void clear() {
        CartLogic.clearCart();
    }
}
