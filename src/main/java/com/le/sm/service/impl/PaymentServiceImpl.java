package com.le.sm.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.le.sm.logic.PaymentLogic;
import com.le.sm.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentLogic logic;

    @Override
    public Map<Integer, Integer> pay(Integer cash) throws Exception {
        return this.logic.pay(cash);
    }

}
