package com.le.sm.service;

import com.le.sm.entity.Product;

public interface CartService {
    /**
     * add || increment product
     *
     * @param product
     */
    public boolean addProduct(Product product);

    /**
     * add || increment product
     *
     * @param product
     * @return
     */
    public boolean increProduct(Product product);

    /**
     * decrement product
     *
     * @param product
     * @return
     */
    public boolean decreProduct(Product product);

    /**
     * remove product
     *
     * @param product
     * @return
     */
    public boolean removeProduct(Product product);

    public void clear();
}
