package com.le.sm.service;

import java.util.Map;

public interface PaymentService {
    /**
     * payment
     *
     * @param cash
     * @throws Exception
     */
    public Map<Integer, Integer> pay(Integer cash) throws Exception;
}
