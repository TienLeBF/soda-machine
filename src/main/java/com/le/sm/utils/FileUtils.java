package com.le.sm.utils;

import java.io.File;

import org.apache.log4j.Logger;


public class FileUtils {
    private static final Logger LOG = Logger.getLogger(FileUtils.class.getSimpleName());

    /**
     * validate file input
     *
     * @param configPath
     * @return
     * @throws Exception
     */
    public static File validate(String configPath) throws Exception {
        if (null == configPath) {
            LOG.error("db config file is null");
            throw new Exception("db config file is null");
        }

        if (configPath.isEmpty()) {
            LOG.error("db config file is empty");
            throw new Exception("db config file is empty");
        }

        File file = new File(configPath);
        if (!file.exists()) {
            LOG.error("db config file is not exist");
            throw new Exception("db config file is not exist");
        }

        if (file.isDirectory()) {
            LOG.error("db config file is directory - not a file");
            throw new Exception("db config file is directory - not a file");
        }

        return file;
    }

    public static String checkFileName(String fullPath) {
        // read in configuration
        String isUsingHdfs = "false";
        final boolean IS_USING_HDFS = Boolean.parseBoolean(isUsingHdfs);
        boolean isExsist = true;
        String fullPathResult = fullPath;
        try {
            int index = 0;
            while (isExsist) {
                // if using hdfs
                if (IS_USING_HDFS) {

                } else {
                    // else not using hdfs
                    File file = new File(fullPathResult);
                    isExsist = file.exists();
                }
                if (isExsist) {
                    // fix file name
                    LOG.info("FileUtils -> checkFileName(String) -> File is exists -> auto get new name");
                    int lastIndex = fullPath.lastIndexOf(".");
                    if (-1 == lastIndex) {
                        LOG.info(
                                "FileUtils -> checkFileName(String) -> File have no extentsion -> auto get new name");
                        lastIndex = fullPath.length() - 1;
                    }
                    fullPathResult = fullPath.substring(0, lastIndex) + "_" + index++ + fullPath.substring(lastIndex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.info("Origin path = " + fullPath);
        LOG.info("New path = " + fullPathResult);
        return fullPathResult;
    }

}
