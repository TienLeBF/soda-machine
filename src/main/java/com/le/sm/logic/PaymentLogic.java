package com.le.sm.logic;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.le.sm.entity.Product;
import com.le.sm.model.Money;
import com.le.sm.utils.Constants;

@Component
public class PaymentLogic {
    private static final Logger LOG = Logger.getLogger(PaymentLogic.class.getSimpleName());

    /**
     * accept note
     *
     * @param money
     * @return
     */
    public static boolean acceptNote(Integer money) {
        boolean isValid = Money.ACCEPT_NOTES.contains(money);
        return isValid;
    }
    /**
     * Payment cart
     * @param cash
     * @throws Exception
     */
    public Map<Integer, Integer> pay(Integer cash) throws Exception {
        try {
            LOG.info("cash: " + cash);
            // calculate money
            int money = 0;
            for (Entry<Product, Integer> item : CartLogic.products.entrySet()) {
                money += item.getKey().getPrice() * item.getValue();
            }
            if (cash < money) {
                throw new Exception(Constants.NOT_ENOUGH_MONEY);
            }
            Map<Integer, Integer> refund = PaymentLogic.refund(Integer.valueOf(cash) - money);
            // if cannot refund -> throw exception
            for (Entry<Product, Integer> item : CartLogic.products.entrySet()) {
                // call hardware pop item
                final int _SIZE = item.getValue();
                for (int i = 0; i < _SIZE; i++) {
                    if (MachineHardwareLogic.popItem(item.getKey())) {
                        // decrement when pop item successful (update cart)
                        CartLogic.products.computeIfPresent(item.getKey(), (k, v) -> item.getValue() - 1);
                    } else {
                        // TODO
                    }
                }
            }
            // update money in machine hardware
            for (Entry<Integer, Integer> item : refund.entrySet()) {
                int SL = Money.MONEY_IN_MACHINE.get(item.getKey());
                // in case of {SL - item.getValue() < 0} => valid in refund
                Money.MONEY_IN_MACHINE.computeIfPresent(item.getKey(), (k, v) -> SL - item.getValue());
            }
            CartLogic.clearCart();
            return refund;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * refund money for customer
     *
     * @param money
     * @return
     * @throws Exception
     */
    public static Map<Integer, Integer> refund(Integer money) throws Exception {
        try {
            Integer refund = Integer.valueOf(0);
            Integer tempMoney = money;
            Map<Integer, Integer> MONEY_IN_MACHINE_SPEND = new HashMap<Integer, Integer>();
            MONEY_IN_MACHINE_SPEND.put(Money.TEN_THOUSAND_VND, 0);
            MONEY_IN_MACHINE_SPEND.put(Money.TWENTY_THOUSAND_VND, 0);
            MONEY_IN_MACHINE_SPEND.put(Money.FIFTY_THOUSAND_VND, 0);
            MONEY_IN_MACHINE_SPEND.put(Money.ONE_HUNDRED_THOUSAND_VND, 0);
            MONEY_IN_MACHINE_SPEND.put(Money.TWO_HUNDRED_THOUSAND_VND, 0);
            for (Integer integer : Money.LIST_MONEY) {
                if (tempMoney <= 0) {
                    break;
                }
                if (tempMoney < integer) {
                    continue;
                }
                final int NUM = Money.MONEY_IN_MACHINE.get(integer);
                final int MOD = tempMoney / integer;
                final int SL = NUM > MOD ? MOD : NUM;
                refund += SL * integer;
                tempMoney -= SL * integer;
//                MONEY_IN_MACHINE_SPEND.put(integer, SL);
                MONEY_IN_MACHINE_SPEND.computeIfPresent(integer, (k, v) -> SL);
            }
            if (0 == tempMoney) {
                System.out.println("MONEY_IN_MACHINE_SPEND: " + MONEY_IN_MACHINE_SPEND);
                return MONEY_IN_MACHINE_SPEND;
            } // else
            throw new Exception("Sorry MACHINE cannot refund -> cancel payment !!!");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Something error happen");
        }
    }

}
