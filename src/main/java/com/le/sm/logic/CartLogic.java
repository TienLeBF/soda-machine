package com.le.sm.logic;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.le.sm.configuration.database.ProductMagament;
import com.le.sm.entity.Product;

@Component
public class CartLogic {
    public static final Map<Product, Integer> products = new HashMap<Product, Integer>();

//    public static Integer price = Integer.valueOf(0);
    public boolean addProduct(Product product) {
        final Integer inStock = ProductMagament.stocks.get(product.getId());
        if (inStock > 0) {
            final Integer inCart = CartLogic.products.get(product);
            if (CartLogic.products.containsKey(product)) {
                CartLogic.products.put(product, inCart + 1);
            } else {
                CartLogic.products.put(product, 1);
            }
            ProductMagament.stocks.put(product.getId(), inStock - 1);
            return true;
        }
        return false;
    }

    public boolean addProduct(Integer id) {
        Product product = ProductMagament.products.get(id);
        return this.addProduct(product);
    }

    public boolean removeProduct(Product product) {
        try {
            final Integer inCart = CartLogic.products.get(product);
            final Integer inStock = ProductMagament.stocks.get(product.getId());
            ProductMagament.stocks.put(product.getId(), inStock + inCart);
            CartLogic.products.remove(product);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean increProduct(Product product) {
        return this.addProduct(product);
    }

    public boolean increProduct(Integer id) {
        Product product = ProductMagament.products.get(id);
        return this.addProduct(product);
    }

    public boolean decreProduct(Product product) {
        final Integer inCart = CartLogic.products.get(product);
        if (inCart <= 0) {
            // Not found product
            return false;
        }
        if (inCart == 1) {
            final Integer inStock = ProductMagament.stocks.get(product.getId());
            ProductMagament.stocks.put(product.getId(), inStock + 1);
            CartLogic.products.remove(product);
            return true;
        }
        final Integer inStock = ProductMagament.stocks.get(product.getId());
        CartLogic.products.put(product, inCart - 1);
        ProductMagament.stocks.put(product.getId(), inStock + 1);
        return true;
    }

    public boolean decreProduct(Integer id) {
        Product product = ProductMagament.products.get(id);
        return this.decreProduct(product);
    }

    public static void clearCart() {
        for (Entry<Product, Integer> item : CartLogic.products.entrySet()) {
            final int id = item.getKey().getId();
            final int sl = item.getValue();
            ProductMagament.stocks.put(item.getKey().getId(), ProductMagament.stocks.get(id) + sl);
        }
        CartLogic.products.clear();
    }

}
